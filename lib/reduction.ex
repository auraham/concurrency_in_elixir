defmodule Reduction do

  def process(pid) do
    receive do
      n -> send(pid, n+1)
    end
  end

  def spawn_process(pid) do
    spawn_link(__MODULE__, :process, [pid])
  end

  def create_chain(n_processes) do
    # Create chain
    {pids, last} = Enum.map_reduce(
      1..n_processes,
      self(),
      fn _item, acc_pid -> 
        new_pid = spawn_process(acc_pid)
        {new_pid, new_pid}
      end
    )

    # Debug
    IO.puts("List of pids")
    Enum.each(pids, fn pid -> IO.puts("#{inspect pid}") end)
  
    # Start reduction
    send(last, 0)

    # Receive result
    receive do
      n -> IO.puts("Result #{n}")
    end
  end

  def start(n_processes) do
    create_chain(n_processes)
  end
end
